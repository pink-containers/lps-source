#!../../bin/linux-x86_64/source

## You may have to change source to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/source.dbd"
source_registerRecordDeviceDriver pdbbase

#epicsEnvSet ("STREAM_PROTOCOL_PATH", "${TOP}/db")
epicsEnvSet ("STREAM_PROTOCOL_PATH", ".")
epicsEnvSet ("ASYNPORT", "L0")
#epicsEnvSet ("DEVIP", "127.0.0.1")
#epicsEnvSet ("DEVPORT", "50123")
#epicsEnvSet ("DEVIP", "192.168.1.100")
#epicsEnvSet ("DEVPORT", "48301")
#epicsEnvSet ("IOCBL", "LPQ")
#epicsEnvSet ("IOCDEV", "source")

## Asyn debug
#var streamDebug 1

#drvAsynIPPortConfigure("${ASYNPORT}","172.17.10.245:80 HTTP",0,0,0)
drvAsynIPPortConfigure("${ASYNPORT}", "${DEVIP}:${DEVPORT}",0,0,0)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asyn,PORT=${ASYNPORT},ADDR=0,IMAX=256,OMAX=256")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("source.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=1")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=2")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=3")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=4")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=5")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=6")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=7")
dbLoadRecords("source-axis.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}, N=8")

## DEBUG
dbLoadRecords("protdebug.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=${ASYNPORT}")

## autosave
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=${IOCBL},DEV=${IOCDEV}")

## Start any sequence programs
#seq sncxxx,"user=epics"

